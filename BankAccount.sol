pragma solidity ^0.4.0;

contract BankAccount{
    //DB(イメージ)に保存する型
    uint amount;
    address manager;
    //リファクタリング Before
    modifier restricted(){
        require(msg.sender == manager);
        _;
    }

    function BankAccount(uint _amount){
        amount = _amount;
        manager = msg.sender;
    }

    function getAmount() public view restricted returns(uint){
        return amount;
    }

    function changeAmount(uint x) public restricted {
     amount = x;
    }
}
